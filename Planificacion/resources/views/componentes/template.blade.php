@extends('layouts.app')

@section('content')
<h6 class="btn btn-secondary btn-lg btn-block">Grupos de II Semestre <br>@php echo ucwords(strtolower($collection[0]->nombrecarrera)) @endphp</h6>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-7">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">Nº</th>
                    <th scope="col">Componente</th>
                    <th scope="col">Año</th>
                    <th scope="col">H.T.</th>
                    <th scope="col">H.P.</th>
                    <th scope="col">Aciones</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($collection as $item)
                        <tr id="color{{$item->id}}">
                        <th width="5%">{{$item->id}}</th>
                            <td width="50%">{{$item->componente}} @if ($item->modalidad == 'VIRTUAL'){{" [VIRTUAL]"}}@endif</td>
                            <td>{{$item->anyo}}</td>
                            <td width="5%"><input id="horasidT{{$item->id}}" type="number" min="0" max="30" value="{{$item->horasteoricas}}"/></td>
                            <td width="5%"><input id="horasidP{{$item->id}}" type="number" min="0" max="30" value="{{$item->horaspracticas}}"/></td>
                            <td><a href="javascript:updatehoursComp({{$item->id}})" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Actualizar Horas"><i class="icon ion-md-sync"></i></a>  <a href="javascript:onSetViewGrup({{$item->id}})" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Grupos"><i class="icon ion-md-construct"></i></a></td>
                          </tr>
                    @endforeach
                </tbody>
              </table>
              <div class="row justify-content-center">{{ $collection->links() }}</div>
        </div>
        <div id="containerGrup" class="col-md-5" hidden style="overflow-y: scroll; height: 460px; border: solid 0px;">
            <h5 id="titleComp"></h5>
            <input type="number" id="idComp" hidden disabled value=""/>
            <input type="number" id="hoursCompT" hidden disabled value=""/>
            <input type="number" id="hoursCompP" hidden disabled value=""/>
            <a href="javascript:onSetGrup(0)" id="actionGrT" class="btn btn-primary btn-sm">GT +</a>
            <a href="javascript:onSetGrup(1)" id="actionGrP" class="btn btn-info btn-sm">GP +</a>
            <br /><br />
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Grupo</th>
                        <th>Horas</th>
                        <th>Docente</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="containerGrp">

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
@routes
<script src="{{ asset('js/script.js') }}"></script>
@endsection