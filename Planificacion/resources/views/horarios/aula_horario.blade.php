@extends('layouts.app')

@section('content')
<div class="container">
    <center>
      <h4>Universidad Nacional Autónoma de Nicaragua</h4>
      <h5>UNAN - León</h5>
      <h6>Departamento de Computación</h6>
      <h6>Horario por Aula {{Session::get('IdSem')}} {{Session::get('IdName')}}</h6>
    </center>
    <!-- Formulario para seleccionar al docente -->
    <form>
        <div class="row">
            <h4 class="col-md-2">Aula: </h4>
            <select class="custom-select col-md-8" id="idaula">
                <option selected disabled value="-1">Selecione...</option>
                <option  value='100'>TODOS</option>
                @foreach ($collection as $item)
                    <option  value='{{$item->id}}'>{{$item->nombre}}</option>
                @endforeach
            </select>
            <div class="pl-1 col-md-2">
                <button type="button" class="col-md-12 btn btn-primary" onclick="onViewAulaH();">Mostrar</button>
            </div>
        </div>
    </form>
    <hr />
    <div class="row" id="containerHAU">
    </div>
</div>
<br />
<br />
@endsection
@section('script')
@routes
<script src="{{ asset('js/scriptHorarios.js') }}"></script>
@endsection