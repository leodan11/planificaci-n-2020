@extends('layouts.app')

@section('content')
<div class="container">
    <center>
      <h4>Universidad Nacional Autónoma de Nicaragua</h4>
      <h5>UNAN - León</h5>
      <h6>Departamento de Computación</h6>
      <h6>Horario por Año {{Session::get('IdSem')}} {{Session::get('IdName')}}</h6>
    </center>
    <!-- Formulario para seleccionar al anyo -->
    <form>
        <div class="row">
            <h4 class="col-md-2">Carrera: </h4>
            <select class="custom-select col-md-4" id="carrera">
                <option selected disabled value="-1">Selecione...</option>
                <option value="IS">INGENIERÍA EN SISTEMAS DE INFORMACIÓN</option>
                <option value="IT">INGENIERÍA EN TELEMÁTICA</option>
            </select>
            <h4 class="col-md-2 text-right">Año: </h4>
            <select class="custom-select col-md-2" id="anyo">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="100">TODOS</option>
            </select>
            <div class="pl-1 col-md-2">
                <button type="button" class="col-md-12 btn btn-primary" onclick="onViewAnyoH();">Mostrar</button>
            </div>
        </div>
    </form>
    <hr />
    <div class="row" id="containerHAY">
    </div>
</div>
<br />
<br />
@endsection
@section('script')
@routes
<script src="{{ asset('js/scriptHorarios.js') }}"></script>
@endsection