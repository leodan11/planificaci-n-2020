@extends('layouts.app')

@section('content')
<h6 class="btn btn-secondary btn-lg btn-block">Aula del Departamento de Computación</h6>
<div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <p class="row justify-content-center"><strong>Acciones</strong>  <i class="icon ion-md-hammer"></i></p>
        <br />
        <div class="list-group">
            <a href="javascript:onKeyAula(0)" class="list-group-item list-group-item-action p-2 text-center">CREAR  <i class="icon ion-md-add-circle"></i></a>
            <a href="" data-toggle="modal" data-target="#exampleModalEditar" class="list-group-item list-group-item-action p-2 text-center">EDITAR  <i class="icon ion-md-create"></i></a>
            <a href="" data-toggle="modal" data-target="#exampleModalEliminar" class="list-group-item list-group-item-action p-2 text-center">ELIMINAR  <i class="icon ion-md-trash"></i></a>
        </div>
    </div>
    <div class="col-md-10">
        <div id="containerViewAulaInicio" class="container">
            
        </div>
        <div id="containerViewAulaCreate" class="container" hidden>
            <div class="row">
              <input type="number" id="idAula" hidden="hidden" disabled="disabled" value="">
                <div class="col-md-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal">
                                <div class="form-group row">
                                    <label for="inputNameAula" class="col-sm-2 control-label">Nombre Aula</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputNameAula" placeholder="B2-JORGE ARGÜELLO">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputTipoAula" class="col-sm-2 control-label">Tipo Aula</label>
                                    <div class="col-sm-8">
                                        <select id="inputTipoAula" class="form-control">
                                            <option selected value="-1">Selecione ...</option>
                                            <option value="1">TEORICA</option>
                                            <option value="2">PRACTICA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputCapacidadAula" class="col-sm-2 control-label">Capacidad</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="capacidad" value="0" class="form-control" id="inputCapacidadAula">
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row" >
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <button onclick="SendAulaInfo()" id="btnSendAula" type="button" class="btn btn-success btn-lg btn-block">Enviar Datos</button>
                                        <button onclick="btnActionAulUpdate()" id="btnUpdateAula" type="button" class="btn btn-primary btn-lg btn-block" hidden>Actualizar Datos</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- Modal Editar-->
<div class="modal fade" id="exampleModalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Aula</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-aulaEdit" class="col-form-label">Nombre Aula:</label>
              <select id="recipient-aulaEdit" class="form-control">
                  <option selected>Selecione ...</option>
                  @foreach ($collection as $item)
                    <option value="{{$item->id}}">{{$item->nombre}}</option>
                  @endforeach
                </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button onclick="btnActionAulEdit()" id="btnActionAulaEdit" type="button" class="btn btn-primary">Editar <i class="icon ion-md-create"></i></button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Eliminar-->
<div class="modal fade" id="exampleModalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Aula Eliminar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-aula-delete" class="col-form-label">Nombre Aula:</label>
              <select id="recipient-aula-delete" class="form-control">
                  <option selected>Selecione ...</option>
                  @foreach ($collection as $item)
                    <option value="{{$item->id}}">{{$item->nombre}}</option>
                  @endforeach
                </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
            <button onclick="btnActionAulDelete()" id="btnActionAulaDelete" type="button" class="btn btn-danger">Si <i class="icon ion-md-trash"></i></button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
@routes
<script src="{{ asset('js/scriptElements.js') }}"></script>
@endsection