@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalLong">
          Grupos no asignados
        </button>
        <br /><br />
        <a href="javascript:onProgressAll(-1)" class="p-1">Ver todos </a> |
        <a href="javascript:onProgressAll(-2)" class="p-1">Colapsar todos </a>
        <br /><br />
        <div id="containerList" class="list-group">
            @foreach ($minimizing as $item)
            <a href="javascript:onProgressMaxD({{$item->id}})" class="list-group-item list-group-item-action p-2">{{$item->nombre}}</a>
            @endforeach
        </div>
      </div>
      <div class="col-md-10">
        <div id="containerTeacher" class='row mb-3'>
          @php
            $count = count($collection);
            $i = 0;
          @endphp
          @while ($i < $count)
            <!-- Desde acá se puede configurar la cantidad de elementos que se deben mostrar -->
            <div class="col-md-3">
              <div class="card p-0 mb-3">
                <div class="card-body p-1">
                  <table class="table">
                    <thead>
                      <tr class="thead bg-light"><th class="p-1">@php echo ucwords(strtolower($collection[$i]['docente']->nombre)) @endphp</th>
                        <th class="p-1">G</th><th colspan="0" class="p-1">H</th>
                        <!-- Evento para minimizar el docente actual -->
                        <th class="p-1"><a href="javascript:onProgressMinD({{$collection[$i]['docente']->id}})"><i class="icon ion-md-remove"></i></a>
                        </th>
                      </tr>
                    </thead>
                    @php
                    // Total de horas que debe cumplir el docente fijas y horarias
                    $horas = $collection[$i]['docente']->horas + $collection[$i]['docente']->horashorario;
                    // Inicialización de las horas asignadas al docente
                    $suma = 0;
                    @endphp
                    @foreach ($collection[$i]['componetes'] as $item)
                          @if ($item->componente != "")
                            <tr>
                              <td width="70%" class="p-1">{{$item->carrera}}-{{$item->componente}}</td>
                                <td class="p-1">G{{$item->tipo}}{{$item->numero}}</td>
                                <td class="p-1">{{$item->horasgrupo}}</td>
                                <td class="p-1"><a href="javascript:onProgressGrupDelete({{$item->id}})"><i class="icon ion-md-trash"></i></a></td>
                            </tr>
                          @endif
                            @php
                                $suma = $suma + $item->horasgrupo;
                            @endphp
                    @endforeach
                  </table>
                </div>
                <!-- Mostraremos -->
                @php
                    if (($horas - $suma) < 0) {
                      echo "<div class='card-footer p-1 text-white text-right bg-danger'>";
                    }else if (($horas - $suma) == 0) {
                      echo "<div class='card-footer p-1 text-white text-right bg-success'>";
                    }else if (($horas - $suma) > 0) {
                      echo "<div class='card-footer p-1 text-white text-right bg-primary'>";
                    }
                    echo "<b>".$suma . " / " . $horas;
                @endphp
                </b>
                </div>
              </div>
            </div>
            @php
                $i++;
            @endphp
          @endwhile
        </div>
      </div>
    </div>
  </div>
  <br />
  <br />
  <!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Componentes Pendientes</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table class="table table-strip">
            <thead><tr><th hidden>Id</th><th>Componente</th><th>Carrera</th><th colspan="2">Horas</th></tr></thead>
            <tbody id="table-tbody">
              @foreach ($slopes as $item)
                <tr>
                  <td class="p-1" hidden>{{$item->id}}</td>
                  <td class="p-1">{{$item->componente}}</td>
                  <td class="p-1">{{$item->nombrecarrera}}</td>
                  <td class="p-1 text-center">{{$item->horasgrupo}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
@endsection
@section('script')
@routes
<script src="{{ asset('js/script.js') }}"></script>
@endsection