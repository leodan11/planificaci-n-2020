//Variables Globales
var idGrupoSeleccionado = -1;
var gruposISeIT = new Array();
gruposISeIT['IS'] = new Array();
gruposISeIT['IT'] = new Array();
gruposISeIT['SEI'] = new Array();
var horariosAulas;
var gruposAll;
var number = 0;
var co = 0;

//Funciones Globales
function capitalizeEachWord(str) { 
  let words = str.split(" "); 
  let arr = []; 
  for (i in words) { 
    temp = words[i].toLowerCase(); 
    temp = temp.charAt(0).toUpperCase() + temp.substring(1); 
    arr.push(temp); 
  }
  return arr.join(" "); 
}

Array.prototype.unique = function(a){
  return function(){
    return this.filter(a)
  }
}(function(a,b,c){
  return c.indexOf(a,b+1) < 0
});

// Reemplaza todas las ocurrencias de una cadena en otra 
function replaceAll(str, find, replace) {
	return str.replace(new RegExp(find, 'g'), replace);
}

//Obtener el dia del horario
function onDay(posicionCelda){
  let day = "";
  if (posicionCelda % 5 == 0) {
    day = "Lunes";
  }else if(posicionCelda == 1 || posicionCelda == 6 || posicionCelda == 11 || posicionCelda == 16 || posicionCelda == 21 ||posicionCelda == 26){
    day = "Martes";
  }else if(posicionCelda == 2 || posicionCelda == 7 || posicionCelda == 12 || posicionCelda == 17 || posicionCelda == 22 ||posicionCelda == 27){
    day = "Miercoles";
  }else if(posicionCelda == 3 || posicionCelda == 8 || posicionCelda == 13 || posicionCelda == 18 || posicionCelda == 23 ||posicionCelda == 28){
    day = "Jueves";
  }else{
    day = "Viernes";
  }
  return day;
}

//Obtener el periodo del horario
function onPeriodoHr(posicionCelda){
  let per = "";
  if(posicionCelda >= 0 && posicionCelda < 5){
    per = "7:00 - 9:00";
  }else if(posicionCelda >= 5 && posicionCelda < 10){
    per = "9:00 - 11:00";
  }else if(posicionCelda >= 10 && posicionCelda < 15){
    per = "11:00 - 1:00";
  }else if(posicionCelda >= 15 && posicionCelda < 20){
    per = "1:00 - 3:00";
  }else if(posicionCelda >= 20 && posicionCelda < 25){
    per = "3:00 - 5:00";
  }else if(posicionCelda >= 25 && posicionCelda < 30){
    per = "5:00 - 7:00";
  }
  return per;
}

//Obtener datos de un grupo
function obtenerGrupo(id){
  //Obtenemos la informacion del grupo selecionado
  let tempG = horariosAulas.grups;
  tempG.forEach(element => {
    if (element.idgrupo == id) {
      objGrupoSeleccionado = element;
    }
  });
  return objGrupoSeleccionado;
}

//Se agrega el evento de clic derecho
$(function(){
    $.contextMenu({
        selector: '.context-menu-one', 
        autoHide: true,
        items: {
            "add": {
                name: "Agregar", 
                icon: "add",
                disabled: function(key , opt){
                  /* Deshabilitamos la opción agregar si no hay grupo seleccionado */
                  if(idGrupoSeleccionado == -1)
                    return true;
                  else
                    return false;
                }
            },
            "delete": {
                name: "Eliminar", 
                icon: "delete",
                disabled: function(key, opt){
                  /* Si la celda no posee un grupo asignado no habilitamos la opción borrar */
                  if(obtenerIdGrupoDelaCeldaDelHorario(this[0]) != null)
                    return false;
                  else
                    return true;
                  }
            },
            "sep1": "---------",
            "quit": {
                name: "Salir", 
                icon: function(){
                return 'context-menu-icon context-menu-icon-quit';
            }}
        },
        callback: function(key, options, e) {
          /* Obtenenmos el id de la celda*/
          let idDeLaCelda = this[0].id;
          let attributes = this[0].attributes;
          let diaDeLaCelda = attributes.dia.value;
          let periodoDeLaCelda = attributes.periodo.value;
          let idDelAula = attributes.idaula.value;
          let posicionCelda = attributes.posicioncelda.value;
          let idDelHorario = idDeLaCelda.replace("celdaHtml","");
          
          let docenteOcupado = false;
          let grupoOcupado = false;
          let anyoOcupado = false;
          //Si la opcion es agregar
          if (key == "add") {
            objGrupoSeleccionado = obtenerGrupo(idGrupoSeleccionado);
            //Sacamos los datos de horario
            let horarioAula = horariosAulas.horarioAula;
            let schedule = horarioAula['schedule'];
            /* Se recorren todas las celdas en busca de choques */
            schedule.forEach(celdaJson => {
              if (celdaJson.dia == diaDeLaCelda && celdaJson.periodo == periodoDeLaCelda) {
                /* IF para validar el docente
                Si el docentes está en otra aula */
                if (celdaJson.iddocente == objGrupoSeleccionado.iddocente && !$('#idChoqueDocente').is(":checked")) {
                  /* Si el docente no es 'NO ASIGNADO' reportamos el choque de docente */
                  if(celdaJson.iddocente != 32){
                    toastr.info("Está ocupado en el aula " + celdaJson.aulanombre,"Docente: " + celdaJson.docentenombre)
                    docenteOcupado = true;
                    return;
                  }
                }
                /* IF para validar el componente  */
                /* Previene que existan grupos simultaneos del mismo componente */
                if(celdaJson.idcomponente == objGrupoSeleccionado.idcomponente && !$("#idChoqueComponente").is(":checked")){
                  toastr.info("Existe otro grupo de " + celdaJson.componente + " al mismo tiempo en el aula " + celdaJson.aulanombre,"Componente");
                  grupoOcupado = true;
                  return;
                }
                /* IF para validar el Año  */
                /* Previene que existan grupos simultaneos del mismo año */
                if(celdaJson.carrera == objGrupoSeleccionado.carrera && celdaJson.anyo == objGrupoSeleccionado.anyo && !$("#idChoqueGrupo").is(":checked"))
                {
                  toastr.info("Existe un grupo de " + celdaJson.componente + " al mismo tiempo en el aula " + celdaJson.aulanombre,"Año");
                  anyoOcupado = true;
                  return;
                }
              }
              /* Se deja de buscar si el docente o el grupo están ocupados */
              if(docenteOcupado || grupoOcupado || anyoOcupado){return;}
            });
            /* Verificamos si el docente o el grupo están ocupados */
            if(docenteOcupado || grupoOcupado || anyoOcupado){return;}
            else{
              /* Agregamos el grupo al horario si ni el docente ni el grupo están ocupados */
              onStartGrupoDelHorario(key, idDelAula, posicionCelda, idDelHorario, objGrupoSeleccionado);
            }
          } else if (key == "delete") {
            let objGrupoSeleccionado = new Object();
            objGrupoSeleccionado = obtenerGrupo(obtenerIdGrupoDelaCeldaDelHorario(this[0]));
            onStartGrupoDelHorario(key, idDelAula, posicionCelda, idDelHorario, objGrupoSeleccionado);
          }
        }
    });

    $('.context-menu-one').on('click', function(e) {
		console.log('clicked', this);
	})
});

//Mostrar un aula
function showAula(id){
  if (co == 0) {
    alert("Se recomienda que al generar un choque.\nColocarlo en el horario en orden. \nGracias!");
    co += 1;
  }
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "id" : id
  };
  $.ajax({
    data: parametros,
    url: route('horario.edit').template,
    type: 'POST',
    success: function (response) {
      horariosAulas = response;
      gruposAll = response.grups;
      let container = document.getElementById('containerView');
      //Informacion de horario y aula
      let horarioAula = response.horarioAula;
      let schedule = horarioAula['schedule'];
      //Limpiamos el contenedor
      container.innerHTML = "";
      //Agregamos las vistas
      let part1 = "<div id='aula" + horarioAula.id + "'>";
      let part2 = "<table><thead><tr class='thead bg-light'><th class='p-1'><h3>Aula: " + horarioAula.name + "</h3></th>";
      let part3 = "<th class='p-1'><button type='button' class='btn btn-info btn-sm' onclick='hideAula()' style='margin-bottom: 10px;'><i class='icon ion-md-remove'></button></th></tr></thead><tbody id='tbodyGp'>";
      let part4 = "</tbody></table><table class='table table-bordered'><thead><tr class='table-dark'><th>Período</th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th></tr></thead><tbody>";
      let part5 = "";
      //Variables locales
      let index = 0;
      let count = 0;
      let periodoC = 0;
      let result = "";
      let periodo = ['7:00-9:00', '9:00-11:00', '11:00-1:00', '1:00-3:00', '3:00-5:00', '5:00-7:00'];
      for (index = 0; index < 30; index++) {
        if ((index % 5) == 0) {
          part5 += "<tr><td width='15%'>" + periodo[periodoC] + "</td>";
          periodoC += 1;
        }
        if (schedule[count] != null) {
          if (index == schedule[count].index){
            result = onSearchCountElement(schedule,schedule[count].index);
            part5 += result;
            count += 1;
          }else{
            part5 += "<td width='15%' class='p-0 celdaHTML context-menu-one small p-1' id='' dia='' periodo='' idaula='" + horarioAula.id + "' posicioncelda='" + index + "'></td>";
          }
        }else{
          part5 += "<td width='15%' class='p-0 celdaHTML context-menu-one small p-1' id='' dia='' periodo='' idaula='" + horarioAula.id + "' posicioncelda='" + index + "'></td>";
        }
        if (((index+1) % 5) == 0) {
          part5 += "</tr>";
        }
      }
      let part6 = "</tbody></table></div>";
      container.innerHTML = part1 + part2  + part3 + part4  + part5 + part6;
      cargarHorariosAulas();
      onSetChangeGrups(response.grups);
      clickGrupo(idGrupoSeleccionado);
    },
    error: function (object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

//Buscar si hay mas de 1 elemento en la misma celda
function onSearchCountElement(array,index){
  let row  = "";
  let head = "<td width='15%'><table><tbody>";
  let footer = "</tbody></table></td>";
  let content = "";
  number = index;
  let test = array.filter(onFil);
  if (test.length > 1){
    test.forEach(element => {
      content += "<tr><td width='15%' class='p-0 celdaHTML context-menu-one small p-1' id='celdaHtml" + element.idhorario + "' dia='" + element.dia + "' periodo='" + element.periodo + "' idaula='" + element.idaula + "' posicioncelda='" + element.index + "'></td></tr>";
    });
    row = head + content + footer;
  }else{
    row = "<td width='15%' class='p-0 celdaHTML context-menu-one small p-1' id='celdaHtml" + test[0].idhorario + "' dia='" + test[0].dia + "' periodo='" + test[0].periodo + "' idaula='" + test[0].idaula + "' posicioncelda='" + test[0].index + "'></td>";
  }
  console.log(row);
  return row;
}

function onFil(element){
  if (element.index == number){
    return element;
  }
}

// Oculta la información de un aula
function hideAula() {
    let container = document.getElementById('containerView');
    if (confirm("Se limpiara la vista de horario. ¿Desea continar?")) {
        container.innerHTML = "";
    }
}

//Cargar los grupos
function onSetChangeGrups(grupos){
    let is = 0;
    let it = 0;
    let sei = 0;
    grupos.forEach(element => {
        //Cargarmos grupos IS
        if (element.carrera == 'IS') {
            gruposISeIT['IS'][is] = element;
            is += 1;
        } else if (element.carrera == 'IT') { //Cargarmos grupos IT
            gruposISeIT['IT'][it] = element;
            it += 1;
        } else if (element.carrera == 'SEI') { //Cargarmos grupos SEI
            gruposISeIT['SEI'][sei] = element;
            sei += 1;
        }
    });
}

// Función para rellenar desde el JSON todos los datos del horario 
function cargarHorariosAulas() {
	$.each(horariosAulas, function(i, horarioAula) {
		$.each(horarioAula.schedule, function(j, celdaJson){
			if(celdaJson.idgrupo != null)
				modificarCeldaHTML(celdaJson);
		});
	});
}


// Función encargada de colocar un Json en una celda 
function modificarCeldaHTML(celdaJson, evento = "add") {
	// Seleccionamos la celda en la que se cargará el Json en base a su Id 
	celdaHTML = $("#celdaHtml" + celdaJson.idhorario);

	// Agregamos el fondo y el contenido a la celda en función de si hay que agregar o quitar contenido 
	if(evento == "add") {
		celdaHTML.addClass('bg-light'); 
		
		// Agregamos el contenido a la celda 
		celdaHTML.html("<b>" + celdaJson.componente + "</b><br>" +
			"G" + celdaJson.tipo + celdaJson.numero + " - " + celdaJson.carrera + " - " + celdaJson.anyo + "<br>" +
			celdaJson.docentenombre + "<br>");
	}
	else {
		celdaHTML.removeClass('bg-light');
		celdaHTML.html("");
	}	
}


// Función para filtrar los componentes por año y muestrarlos
function filtrarGrupos(carrera, anyo, iddiv) {
  var gruposAnyo = new Array();
  if (gruposISeIT[carrera] != 0){
    if (gruposISeIT[carrera] == 0 && anyo != 0) {
      toastr.info("Selecione una aula para ver los grupos","Información"); 
    }
    if(anyo == 0)
      gruposAnyo = gruposISeIT[carrera];
    else{
      let i = 0;
      for (let index = 0; index < gruposISeIT[carrera].length; index++) {
        if (gruposISeIT[carrera][index].anyo == anyo){
            gruposAnyo[i] = gruposISeIT[carrera][index];
            i += 1;
        } 
      }
    }
    
    // Vaciamos el div correspondiente para cada listado de grupos 
    $(iddiv).html("");
    // Por cada grupo creamos un botón para poder seleccionarlo 
    $.each(gruposAnyo, function(i, grupo) {
      $(iddiv).append(crearBotonGrupo(grupo));
    });
  }else{
    toastr.info("No existen grupos creados","Información");
  }
}

/* Crea cada uno de los botones de los grupos */
function crearBotonGrupo(grupo) {
	/* Iniciamos a construir el botón del grupo */
	let botonGrupo = "<button id='grupo" + grupo.idgrupo + "' type='button' style='font-size:smaller;' class='mb-1 col-md-12 btn ";

	/* Determinamos cuantos horas están pendientes de planificar */
	let horasAgendadas = horasAgendadasPorGrupo(grupo);
	let horasPendientes = grupo.horasgrupo - horasAgendadas;

	/* Si no hay horas pendientes no hay nada que planificar */
	if(horasPendientes == 0 )
		botonGrupo += "btn-success' ondblclick='dblclickGrupo(" + grupo.idgrupo + ")' >";
	/* Indicamos si por alguna razón existen más horas planificadas de las que se debe */
	else if(horasAgendadas > grupo.horasgrupo)
		botonGrupo += "btn-danger' ondblclick='dblclickGrupo(" + grupo.idgrupo + ")' >";
	/* Indicamos si las horas pendientes son mayores que 0 pero menor que el total del grupo*/
	else if (horasPendientes > 0 && horasPendientes < grupo.horasgrupo)
		botonGrupo += "btn-warning' onclick='clickGrupo(" + grupo.idgrupo + ")' ondblclick='dblclickGrupo(" + grupo.idgrupo + ")' >";
	/* En caso contrario no se han planificado nonguna hora */
	else
		botonGrupo += "btn-outline-info' onclick='clickGrupo(" + grupo.idgrupo + ")' >";

  /* Completamos el contenido del botón */
	botonGrupo = botonGrupo + grupo.componente + "<br>";
	botonGrupo = botonGrupo +  "G" + grupo.tipo + grupo.numero +  " " + horasAgendadas + "/" + grupo.horasgrupo + "<br>";
	botonGrupo = botonGrupo + grupo.docentenombre + " </button>";
	return botonGrupo;
}

// Determina si un grupo se encuentra planificado
function horasAgendadasPorGrupo(grupo)
{
	let aparicionesEnAgenda = 0;

	$.each(horariosAulas, function(i, horarioA) {
		$.each(horarioA.schedule, function(j, celdaJson) {
			if(celdaJson.idgrupo == grupo.idgrupo)
				aparicionesEnAgenda++;
		});			
	});
	return aparicionesEnAgenda * 2;
}

// Función que muestra el aula del grupo al hacer doble clic sobre él
function dblclickGrupo(idgrupo) {
}


// Función a ejecutarse al hacer clic sobre un grupo 
function clickGrupo(idgrupo) {
  if (idgrupo != -1) {
    idGrupoSeleccionado = idgrupo;
    let container = document.getElementById("tbodyGp");
    container.innerHTML = "";
    let part1 = "<tr class='bg-light'><th class='p-1'><h6><b>Grupo Selecionado: " + $("#grupo" + idgrupo).html() + " </b></h6></th>";
    let part2 = "<th class='p-1'><button type='button' class='btn btn-danger btn-sm' onclick='oncloseGP()' style='margin-bottom: 10px;'><i class='icon ion-md-close-circle'></button></th></tr>";
    container.innerHTML = part1 + part2;
  }
}

//Limpiar Grupo
function oncloseGP(){
  idGrupoSeleccionado = -1;
  let container = document.getElementById("tbodyGp");
  container.innerHTML = "";
}

// Obtener el Id del horario del grupo actual de la celda actual
function obtenerIdGrupoDelaCeldaDelHorario(celdaSeleccionada) {
	let attributes = celdaSeleccionada.attributes;
	let idDelAula = attributes.idaula.value;
  let posicionCelda = attributes.posicioncelda.value;
  let horarioAula = horariosAulas.horarioAula;
  let schedule = horarioAula['schedule'];
  if (schedule != null){
    for (let index = 0; index < schedule.length; index++) {
      if(schedule[index].index == posicionCelda){
        return schedule[index].idgrupo;
      }
    }
  }else{
    return null;
  }
}

//Función para agregar el grupo a la celda del aula
function onStartGrupoDelHorario(evento, idDelAula, posicionCelda, idDelHorario, objGrupoSeleccionado){
  /* Almacenamos el Id del grupo */
  let idgrupoSeleccionado = objGrupoSeleccionado.idgrupo;
  /* Actualizamos los datos id vía AJAX */
  let parametros;
  if (evento == "add") {
    parametros = {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "evento" : "add",
      "idGrupo" : idgrupoSeleccionado,
      "idAula" : idDelAula,
      "index" : posicionCelda,
      "dia" : onDay(posicionCelda),
      "periodo" : onPeriodoHr(posicionCelda), 
    };
  }else if (evento == "delete") {
    parametros = {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "evento" : "delete",
      "idHorario" : idDelHorario,
      "idGrupo" : idgrupoSeleccionado,
      "idAula" : idDelAula,
      "dia" : onDay(posicionCelda),
      "periodo" : onPeriodoHr(posicionCelda)
    };
  }

  $.ajax({
    data: parametros,
    url: route('horario.start').template,
    type: 'POST',
  success: function (response) {
    toastr.success(response.message,response.title);
    showAula(response.aula);
    /* Actualizamos los el color del elemento en los grupos */
    let objGrupo;
    botonGrupo = $("#grupo" + response.grupo);
    gruposAll.forEach(element => {
      if (element.idgrupo == response.grupo){
        objGrupo = element;
      }
    });
		botonGrupo.replaceWith(crearBotonGrupo(objGrupo));
  },
  error: function (object,type,message){
    if (type == "error"){
      toastr.error(message,capitalizeEachWord(type));
    }else if (type == "timeout"){
      toastr.info(message,capitalizeEachWord(type));
    }else{
      toastr.warning(message,capitalizeEachWord(type));
    }
  }
  });
}

//Mostrar Horario de Docente
function onViewDocenteH(){
  let id = $('#iddocente').val();
  let container = document.getElementById('containerHD');
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "iddocente" : id
  };
  $.ajax({
    data: parametros,
    url: route('horario.templadeD').template,
    type: 'POST',
  success: function (response) {
    let part0 = "";
    let part1 = "<table class='table table-bordered'><tr class='table-dark'><th>Período</th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th></tr>";
    let contData = "";
    let dia = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'];
    let periodo = ['7:00 - 9:00', '9:00 - 11:00', '11:00 - 1:00', '1:00 - 3:00', '3:00 - 5:00', '5:00 - 7:00'];
    let arrayDato = response.collection;
    let data;
    console.log(response);
    container.innerHTML = "";
    arrayDato.forEach(element => {
      if(arrayDato.length > 1){ part0 = "<h6> " + element.nombre + "</h6>"; }
      data = element.data;
      console.log("Nombre: " + element.nombre + " Data: " + element.data.length);
      for (let item = 0; item < 30; item++) {
        let indiceDia = item % 5;
        let indicePeriodo = Math.trunc(item / 5);
          if ((item % 5) == 0) {
            contData += "<tr><td width='10%'>" + periodo[indicePeriodo ] + "</td>";
          }
          contData += "<td width='15%' class='p-1'>";
          for (let i = 0; i < element.data.length; i++) {
            if (element.data[i].dia == dia[indiceDia] && element.data[i].periodo == periodo[indicePeriodo]) {
              contData += "<b>" + element.data[i].componente + "</b> - G" + element.data[i].tipo + element.data[i].numero + "<br>";
              contData += element.data[i].nombrecarrera + " - " + element.data[i].anyo + " Año<br>";
              contData += "AULA: " + element.data[i].aulanombre + "<br><br>";
              contData += "</td>";   
            }
          }
          contData += "</td>";
          if (((item+1) % 5) == 0) {
            contData += "</tr>";
          }
      }
      container.innerHTML += part0 + part1 + contData + "</table>";
      contData = "";
    });
  },
  error: function (object,type,message){
    if (type == "error"){
      toastr.error(message,capitalizeEachWord(type));
    }else if (type == "timeout"){
      toastr.info(message,capitalizeEachWord(type));
    }else{
      toastr.warning(message,capitalizeEachWord(type));
    }
  }
  });
}

//Mostrar Horario de Anyo
function onViewAnyoH(){
  let carrera = $('#carrera').val();
  let anyo = $('#anyo').val();
  let container = document.getElementById('containerHAY');
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "carrera" : carrera,
    "anyo" : anyo
  };
  console.log(parametros);
  $.ajax({
    data: parametros,
    url: route('horario.viewA').template,
    type: 'POST',
  success: function (response) {
    let part0 = "";
    let part1 = "<table class='table table-bordered'><tr class='table-dark'><th>Período</th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th></tr>";
    let contData = "";
    let dia = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'];
    let periodo = ['7:00 - 9:00', '9:00 - 11:00', '11:00 - 1:00', '1:00 - 3:00', '3:00 - 5:00', '5:00 - 7:00'];
    let arrayDato = response.collection;
    let data;
    console.log(response);
    container.innerHTML = "";
    arrayDato.forEach(element => {
      if(arrayDato.length > 1){ part0 = "<h6>Año: " + element.id + "</h6>"; }
      data = element.data;
      console.log("Nombre: " + element.nombre + " Data: " + element.data.length);
      for (let item = 0; item < 30; item++) {
        let indiceDia = item % 5;
        let indicePeriodo = Math.trunc(item / 5);
          if ((item % 5) == 0) {
            contData += "<tr><td width='10%'>" + periodo[indicePeriodo ] + "</td>";
          }
          contData += "<td width='15%' class='p-1'>";
          for (let i = 0; i < element.data.length; i++) {
            if (element.data[i].dia == dia[indiceDia] && element.data[i].periodo == periodo[indicePeriodo]) {
              contData += "<b>" + element.data[i].componente + "</b> - G" + element.data[i].tipo + element.data[i].numero + "<br>";
              contData += "<b>DOCENTE:</b> " + element.data[i].docentenombre + "<br>";
              contData += "<b>AULA:</b>  " + element.data[i].aulanombre + "<br><br>";
              contData += "</td>";   
            }
          }
          contData += "</td>";
          if (((item+1) % 5) == 0) {
            contData += "</tr>";
          }
      }
      container.innerHTML += part0 + part1 + contData + "</table>";
      contData = "";
    });
  },
  error: function (object,type,message){
    if (type == "error"){
      toastr.error(message,capitalizeEachWord(type));
    }else if (type == "timeout"){
      toastr.info(message,capitalizeEachWord(type));
    }else{
      toastr.warning(message,capitalizeEachWord(type));
    }
  }
  });
}

//Mostrar Horario de Aula
function onViewAulaH(){
  let id = $('#idaula').val();
  let container = document.getElementById('containerHAU');
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "idaula" : id
  };
  $.ajax({
    data: parametros,
    url: route('horario.viewU').template,
    type: 'POST',
  success: function (response) {
    let part0 = "";
    let part1 = "<table class='table table-bordered'><tr class='table-dark'><th>Período</th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th></tr>";
    let contData = "";
    let dia = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'];
    let periodo = ['7:00 - 9:00', '9:00 - 11:00', '11:00 - 1:00', '1:00 - 3:00', '3:00 - 5:00', '5:00 - 7:00'];
    let arrayDato = response.collection;
    let data;
    console.log(response);
    container.innerHTML = "";
    arrayDato.forEach(element => {
      if(arrayDato.length > 1){ part0 = "<h6> " + element.nombre + "</h6>"; }
      data = element.data;
      console.log("Nombre: " + element.nombre + " Data: " + element.data.length);
      for (let item = 0; item < 30; item++) {
        let indiceDia = item % 5;
        let indicePeriodo = Math.trunc(item / 5);
          if ((item % 5) == 0) {
            contData += "<tr><td width='10%'>" + periodo[indicePeriodo ] + "</td>";
          }
          contData += "<td width='15%' class='p-1'>";
          for (let i = 0; i < element.data.length; i++) {
            if (element.data[i].dia == dia[indiceDia] && element.data[i].periodo == periodo[indicePeriodo] && element.data[i].idgrupo != 0 && element.data[i].idgrupo != null) {
              contData += "<b>" + element.data[i].componente + "</b> - G" + element.data[i].tipo + element.data[i].numero + "<br>";
              contData += "<b>DOCENTE:</b> " + element.data[i].docentenombre + "<br>";
              contData += "<b>AULA:</b>  " + element.data[i].aulanombre + "<br><br>";  
            }
          }
          contData += "</td>";
          if (((item+1) % 5) == 0) {
            contData += "</tr>";
          }
      }
      container.innerHTML += part0 + part1 + contData + "</table>";
      contData = "";
    });
  },
  error: function (object,type,message){
    if (type == "error"){
      toastr.error(message,capitalizeEachWord(type));
    }else if (type == "timeout"){
      toastr.info(message,capitalizeEachWord(type));
    }else{
      toastr.warning(message,capitalizeEachWord(type));
    }
  }
  });
}