//Variables Globales

//Funciones Globales
function capitalizeEachWord(str) { 
    let words = str.split(" "); 
    let arr = []; 
    for (i in words) { 
      temp = words[i].toLowerCase(); 
      temp = temp.charAt(0).toUpperCase() + temp.substring(1); 
      arr.push(temp); 
    }
    return arr.join(" "); 
} 

//Año
function onStart(key){
    let container = document.getElementById('containerViewAnyo');
    switch (key) {
        case 1:
            container.hidden = true;
            break;
        case 2:
            container.hidden = false;
            break;
    }
}

//Crear un año lectivo
function onCreateYear(){
    let inputYear = document.getElementById('validationCustom01').value;
    let selectSem = document.getElementById('validationCustom03').value;
    if (inputYear == '' || selectSem == ''){
        toastr.warning("Verifique los datos ingresados!.","Error");
    }else{
        let parametros = {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "year" : inputYear,
            "semest" : selectSem
        };
        $.ajax({
            data: parametros,
            url: route('anyos.create').template,
            type: 'POST',
            success: function (response) {
                toastr.success('Se ha creado exitosamente', "Ciclo: " + response.message);
            },
            error: function (object,type,message){
                if (type == "error"){
                    toastr.error(message,capitalizeEachWord(type));
                }else if (type == "timeout"){
                    toastr.info(message,capitalizeEachWord(type));
                }else{
                    toastr.warning(message,capitalizeEachWord(type));
                }
            }
        });
    }
}

//Docente
function onKeyDocente(key){
    let containerHome = document.getElementById('containerViewDocInicio');
    let containerCreate = document.getElementById('containerViewDocCreate');
    let name = document.getElementById('inputNameDocente');
    let horas = document.getElementById('inputHorasDocente');
    let horasHor= document.getElementById('inputHorasHorarioDocente');
    let contratacion = document.getElementById('inputContratacion');
    let containerBtnCreate = document.getElementById('btnSend');
    let containerBtnUpdate = document.getElementById('btnUpdate');
    switch (key) {
        case 0:
            name.value = "";
            horas.value = 0;
            horasHor.value = 0;
            contratacion.value = -1;
            containerBtnCreate.hidden = false;
            containerBtnUpdate.hidden = true;
            containerCreate.hidden = false;
            containerHome.hidden = true;
            break
    }
}

//Refrescar lista Docente
function onRefrestD(){
    let dataEd = document.getElementById('recipient-nameEdit');
    let dataDe = document.getElementById('recipient-name-delete');
    dataEd.innerHTML = "";
    dataDe.innerHTML = "";
    let part1 = "<option selected>Selecione ...</option>";
    let part2 = "";
    let element = null;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
    };
    $.ajax({
        data: parametros,
        url: route('docentes.all').template,
        type: 'POST',
        success: function (response) {
            element = response.collection;
            for (let index = 0; index < element.length; index++) {
                part2 += "<option value='" + element[index].id + "'>" + element[index].nombre + "</option>";
            }
            dataEd.innerHTML = part1 + part2;
            dataDe.innerHTML = part1 + part2;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
}

//Agregar un docente
function btnSendDoc(){
    let name = document.getElementById('inputNameDocente').value;
    let horas = document.getElementById('inputHorasDocente').value;
    let horasHor= document.getElementById('inputHorasHorarioDocente').value;
    let contratacion = document.getElementById('inputContratacion').value;
    let contratacionText = "";
    if (contratacion == 1) {    
        contratacionText = "DOCENTE";
    } else if (contratacion == 2) {
        contratacionText = "HORARIO";
    } else if (contratacion == 3){
        contratacionText = "ADMINISTRATIVO";
    }
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "name" : name,
        "horas" : horas,
        "horasHor" : horasHor,
        "contratacion" : contratacionText
    };
    $.ajax({
        data: parametros,
        url: route('docentes.create').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha creado con exito!', "Docente: " + response.name);
            onRefrestD();
            document.getElementById('containerViewDocInicio').hidden = false;
            document.getElementById('containerViewDocCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
}

//Editar Docente
function btnActionDocEdit(){
    let containerCreate = document.getElementById('containerViewDocCreate');
    let containerBtnCreate = document.getElementById('btnSend');
    let containerBtnUpdate = document.getElementById('btnUpdate');
    let id = document.getElementById('recipient-nameEdit').value;
    document.getElementById('idDocente').value = id;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id
    };
    $.ajax({
        data: parametros,
        url: route('docentes.search').template,
        type: 'POST',
        success: function (response) {
            docente = response.docente;
            document.getElementById('inputNameDocente').value = docente.nombre;
            document.getElementById('inputHorasDocente').value = docente.horas;
            document.getElementById('inputHorasHorarioDocente').value = docente.horashorario;
            let contratacion = docente.contratacion;
            switch(contratacion){
                case "DOCENTE":
                    document.getElementById('inputContratacion').value = 1;
                    break;
                case "HORARIO":
                    document.getElementById('inputContratacion').value = 2;
                    break;
                case "ADMINISTRATIVO":
                    document.getElementById('inputContratacion').value = 3;
            }
            containerBtnCreate.hidden = true;
            containerBtnUpdate.hidden = false;
            containerCreate.hidden = false;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
    $('#exampleModalEditar').modal('hide');
}

//Actualizar Docente
function btnActionDocUpdate(){
    let id = document.getElementById('idDocente').value;
    let name = document.getElementById('inputNameDocente').value;
    let horas = document.getElementById('inputHorasDocente').value;
    let horasHor= document.getElementById('inputHorasHorarioDocente').value;
    let contratacion = document.getElementById('inputContratacion').value;
    let contratacionText = "";
    if (contratacion == 1) {    
        contratacionText = "DOCENTE";
    } else if (contratacion == 2) {
        contratacionText = "HORARIO";
    } else if (contratacion == 3){
        contratacionText = "ADMINISTRATIVO";
    }
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id,
        "name" : name,
        "horas" : horas,
        "horasHor" : horasHor,
        "contratacion" : contratacionText
    };
    $.ajax({
        data: parametros,
        url: route('docentes.update').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha actualizado con exito!', "Docente: " + response.name);
            onRefrestD();
            document.getElementById('containerViewDocInicio').hidden = false;
            document.getElementById('containerViewDocCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
}

//Eliminar Docente
function btnActionDocDelete(){
    let id = document.getElementById('recipient-name-delete').value;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id
    };
    $.ajax({
        data: parametros,
        url: route('docentes.delete').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha eliminado con exito!', "Docente: " + response.name);
            onRefrestD();
            document.getElementById('containerViewDocInicio').hidden = false;
            document.getElementById('containerViewDocCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
    $('#exampleModalEliminar').modal('hide');
}

//Aula
function onKeyAula(key){
    let containerHome = document.getElementById('containerViewAulaInicio');
    let containerCreate = document.getElementById('containerViewAulaCreate');
    let containerBtnCreate = document.getElementById('btnSendAula');
    let containerBtnUpdate = document.getElementById('btnUpdateAula');
    document.getElementById('inputNameAula').value = "";
    document.getElementById('inputTipoAula').value = -1;
    document.getElementById('inputCapacidadAula').value = 0;
        switch (key) {
        case 0:
            containerBtnCreate.hidden = false;
            containerBtnUpdate.hidden = true;
            containerCreate.hidden = false;
            containerHome.hidden = true;
            break
    }
}

//Agregar una Aula
function SendAulaInfo(){
    let name = document.getElementById('inputNameAula').value;
    let capacidad = document.getElementById('inputCapacidadAula').value;
    let tipo = document.getElementById('inputTipoAula').value;
    let tipoAula = "";
    if (tipo == 1){
        tipoAula = "T";
    } else if (tipo == 2){
        tipoAula = "P";
    }
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "name" : name,
        "tipo" : tipoAula,
        "capacidad" : capacidad
    };
    $.ajax({
        data: parametros,
        url: route('aulas.create').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha creado con exito!', "Aula: " + response.name);
            onRefrestAula(response.elements);
            document.getElementById('containerViewAulaInicio').hidden = false;
            document.getElementById('containerViewAulaCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
}

//Editar Aula
function btnActionAulEdit(){
    let containerInicio = document.getElementById('containerViewAulaInicio');
    let containerCreate = document.getElementById('containerViewAulaCreate');
    let containerBtnCreate = document.getElementById('btnSendAula');
    let containerBtnUpdate = document.getElementById('btnUpdateAula');
    let id = document.getElementById('recipient-aulaEdit').value;
    document.getElementById('idAula').value = id;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id
    };
    $.ajax({
        data: parametros,
        url: route('aulas.search').template,
        type: 'POST',
        success: function (response) {
            let aula = response.aula;
            document.getElementById('inputNameAula').value = aula.nombre;
            document.getElementById('inputCapacidadAula').value = aula.capacidad;
            let tipo = aula.tipo;
            if (tipo == "T") {
                document.getElementById('inputTipoAula').value = 1;
            }else if (tipo == "P"){
                document.getElementById('inputTipoAula').value = 2;
            }else{
                document.getElementById('inputTipoAula').value = -1;
            }
            containerBtnCreate.hidden = true;
            containerBtnUpdate.hidden = false;
            containerCreate.hidden = false;
            containerInicio.hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
    $('#exampleModalEditar').modal('hide');
}

//Actualizar Aula
function btnActionAulUpdate(){
    let id = document.getElementById('idAula').value;
    let name = document.getElementById('inputNameAula').value;
    let capacidad = document.getElementById('inputCapacidadAula').value;
    let tipo = document.getElementById('inputTipoAula').value;
    let tipoAula = "";
    if (tipo == 1){
        tipoAula = "T";
    } else if (tipo == 2){
        tipoAula = "P";
    }
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id,
        "name" : name,
        "tipo" : tipoAula,
        "capacidad" : capacidad
    };
    $.ajax({
        data: parametros,
        url: route('aulas.update').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha actualizado con exito!', "Aula: " + response.name);
            document.getElementById('containerViewAulaInicio').hidden = false;
            document.getElementById('containerViewAulaCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
}

//Eliminar Aula
function btnActionAulDelete(){
    let id = document.getElementById('recipient-aula-delete').value;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id
    };
    $.ajax({
        data: parametros,
        url: route('aulas.delete').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha eliminado con exito!', "Aula: " + response.name);
            onRefrestAula(response.elements);
            document.getElementById('containerViewAulaInicio').hidden = false;
            document.getElementById('containerViewAulaCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
    $('#exampleModalEliminar').modal('hide');
}

//Actulaizar lista de Aulas
function onRefrestAula(element){
    let containerEditA = document.getElementById('recipient-aulaEdit');
    let containerDelA = document.getElementById('recipient-aula-delete');
    containerEditA.innerHTML = "";
    containerDelA.innerHTML = "";
    let part1 = "<option selected>Selecione ...</option>";
    let part2 = "";
    for (let index = 0; index < element.length; index++) {
        part2 += "<option value='" + element[index].id + "'>" + element[index].nombre + "</option>";
    }
    containerEditA.innerHTML = part1 + part2;
    containerDelA.innerHTML = part1 + part2;
}

//Componente
function onKeyComponente(key){
    let containerHome = document.getElementById('containerViewCompInicio');
    let containerCreate = document.getElementById('containerViewCompCreate');
    //let containerBtnCreate = document.getElementById('btnSend');
    //let containerBtnUpdate = document.getElementById('btnUpdate');
    switch (key) {
        case 0:
            //document.getElementById('inputNameDocente').value = "";
            //document.getElementById('inputHorasDocente').value = 0;
            //document.getElementById('inputHorasHorarioDocente').value = 0;
            //document.getElementById('inputContratacion').value = -1;
            //containerBtnCreate.hidden = false;
            //containerBtnUpdate.hidden = true;
            containerCreate.hidden = false;
            containerHome.hidden = true;
            break;
    }
}

//Agregar un componete
function onSendDataComp(){
    let nombreComp = document.getElementById('inputNameComponente').value;
    let modalidad = document.getElementById('inputComponenteModalidad').value;
    let modalidadText = "";
    if (modalidad == 1){
        modalidadText = "PRESENCIAL";
    }else if (modalidad == 2){
        modalidadText = "VIRTUAL";
    }
    let horasTeo = document.getElementById('inputHorasTeoricasComp').value;
    let horasPra = document.getElementById('inputHorasPracticasComp').value;
    let carrera = "";
    let carreraSigla = "";
    if (document.getElementById('gridRadios1').checked){
        carrera = "INGENIERIA EN SISTEMAS DE INFORMACION";
        carreraSigla = "IS";
    }else if (document.getElementById('gridRadios2').checked){
        carrera = "INGENIERIA EN TELEMATICA";
        carreraSigla = "IT";
    }else if (document.getElementById('gridRadios3').checked){
        carrera = "TECNICO SUPERIOR EN DESARROLLO WEB";
        carreraSigla = "UA";
    }else if (document.getElementById('gridRadios4').checked){
        carrera = "OFERTA OPTATIVAS DE LA FFCCTT";
        carreraSigla = "SE";
    }else if (document.getElementById('gridRadios5').checked){
        carrera = "TS ESTADISTICA DE SALUD";
        carreraSigla = "SEI";
    }else if (document.getElementById('gridRadios6').checked){
        carrera = "ENFERMERIA";
        carreraSigla = "SEI";
    }else if (document.getElementById('gridRadios7').checked){
        carrera = "BIOANALISIS CLINICO";
        carreraSigla = "SEI";
    }else if (document.getElementById('gridRadios8').checked){
        carrera = "BIOLOGIA";
        carreraSigla = "SEI";
    }
    let ciclo = document.getElementById('inputCicloComp').value;
    let anyo = document.getElementById('inputYearComp').value;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "nombreC" : nombreComp,
        "modalidad" : modalidadText,
        "horasT" : horasTeo,
        "horasP" : horasPra,
        "carrera" : carrera,
        "siglas" : carreraSigla,
        "ciclo" : ciclo,
        "anyo" : anyo
    };
    $.ajax({
        data: parametros,
        url: route('componentes.create').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha creado con exito!', "Componente: " + response.name);
            onRefreshCompList(response.elements);
            document.getElementById('containerViewCompInicio').hidden = false;
            document.getElementById('containerViewCompCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
}

//Editar Componente
function btnActionCompEdit(){
    document.getElementById('containerViewCompInicio').hidden = true;
    let containerCreate = document.getElementById('containerViewCompCreate');
    let containerBtnCreate = document.getElementById('btnSendComp');
    let containerBtnUpdate = document.getElementById('btnUpdateComp');
    let id = document.getElementById('recipient-CompEdit').value;
    document.getElementById('idComponente').value = id;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id
    };
    $.ajax({
        data: parametros,
        url: route('componentes.search').template,
        type: 'POST',
        success: function (response) {
            onChargeComponente(response.componente);
            containerBtnCreate.hidden = true;
            containerBtnUpdate.hidden = false;
            containerCreate.hidden = false;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
    $('#exampleModalEditar').modal('hide');
}

//Cargar componente en la vista
function onChargeComponente(componente) {
    document.getElementById('inputNameComponente').value = componente.componente;
    if (componente.modalidad == "PRESENCIAL"){
        document.getElementById('inputComponenteModalidad').value = 1;
    }else if (componente.modalidad == "VIRTUAL"){
        document.getElementById('inputComponenteModalidad').value = 2;
    }else{
        document.getElementById('inputComponenteModalidad').value = -1;
    }
    document.getElementById('inputHorasTeoricasComp').value = componente.horasteoricas;
    document.getElementById('inputHorasPracticasComp').value = componente.horaspracticas;
    if (componente.nombrecarrera = "INGENIERIA EN SISTEMAS DE INFORMACION"){
        document.getElementById('gridRadios1').checked = true;
        document.getElementById('gridRadios2').checked = false;
        document.getElementById('gridRadios3').checked = false;
        document.getElementById('gridRadios4').checked = false;
        document.getElementById('gridRadios5').checked = false;
        document.getElementById('gridRadios6').checked = false;
        document.getElementById('gridRadios7').checked = false;
        document.getElementById('gridRadios8').checked = false;
    }else if (componente.nombrecarrera = "INGENIERIA EN TELEMATICA"){
        document.getElementById('gridRadios1').checked = false;
        document.getElementById('gridRadios2').checked = true;
        document.getElementById('gridRadios3').checked = false;
        document.getElementById('gridRadios4').checked = false;
        document.getElementById('gridRadios5').checked = false;
        document.getElementById('gridRadios6').checked = false;
        document.getElementById('gridRadios7').checked = false;
        document.getElementById('gridRadios8').checked = false;
    }else if (componente.nombrecarrera = "TECNICO SUPERIOR EN DESARROLLO WEB"){
        document.getElementById('gridRadios1').checked = false;
        document.getElementById('gridRadios2').checked = false;
        document.getElementById('gridRadios3').checked = true;
        document.getElementById('gridRadios4').checked = false;
        document.getElementById('gridRadios5').checked = false;
        document.getElementById('gridRadios6').checked = false;
        document.getElementById('gridRadios7').checked = false;
        document.getElementById('gridRadios8').checked = false;
    }else if (componente.nombrecarrera = "OFERTA OPTATIVAS DE LA FFCCTT"){
        document.getElementById('gridRadios1').checked = false;
        document.getElementById('gridRadios2').checked = false;
        document.getElementById('gridRadios3').checked = false;
        document.getElementById('gridRadios4').checked = true;
        document.getElementById('gridRadios5').checked = false;
        document.getElementById('gridRadios6').checked = false;
        document.getElementById('gridRadios7').checked = false;
        document.getElementById('gridRadios8').checked = false;
    }else if (componente.nombrecarrera = "TS ESTADISTICA DE SALUD"){
        document.getElementById('gridRadios1').checked = false;
        document.getElementById('gridRadios2').checked = false;
        document.getElementById('gridRadios3').checked = false;
        document.getElementById('gridRadios4').checked = false;
        document.getElementById('gridRadios5').checked = true;
        document.getElementById('gridRadios6').checked = false;
        document.getElementById('gridRadios7').checked = false;
        document.getElementById('gridRadios8').checked = false;
    }else if (componente.nombrecarrera = "ENFERMERIA"){
        document.getElementById('gridRadios1').checked = false;
        document.getElementById('gridRadios2').checked = false;
        document.getElementById('gridRadios3').checked = false;
        document.getElementById('gridRadios4').checked = false;
        document.getElementById('gridRadios5').checked = false;
        document.getElementById('gridRadios6').checked = true;
        document.getElementById('gridRadios7').checked = false;
        document.getElementById('gridRadios8').checked = false;
    }else if (componente.nombrecarrera = "BIOANALISIS CLINICO"){
        document.getElementById('gridRadios2').checked = false;
        document.getElementById('gridRadios3').checked = false;
        document.getElementById('gridRadios4').checked = false;
        document.getElementById('gridRadios5').checked = false;
        document.getElementById('gridRadios6').checked = false;
        document.getElementById('gridRadios7').checked = true;
        document.getElementById('gridRadios8').checked = false;
    }else if (componente.nombrecarrera = "BIOLOGIA"){
        document.getElementById('gridRadios1').checked = false;
        document.getElementById('gridRadios2').checked = false;
        document.getElementById('gridRadios3').checked = false;
        document.getElementById('gridRadios4').checked = false;
        document.getElementById('gridRadios5').checked = false;
        document.getElementById('gridRadios6').checked = false;
        document.getElementById('gridRadios7').checked = false;
        document.getElementById('gridRadios8').checked = true;
    }
    document.getElementById('inputCicloComp').value = componente.ciclo;
    document.getElementById('inputYearComp').value = componente.anyo;
}

//Actualizar Componente
function onUpdateDataComp(){
    let id = document.getElementById('idComponente').value;
    let nombreComp = document.getElementById('inputNameComponente').value;
    let modalidad = document.getElementById('inputComponenteModalidad').value;
    let modalidadText = "";
    if (modalidad == 1){
        modalidadText = "PRESENCIAL";
    }else if (modalidad == 2){
        modalidadText = "VIRTUAL";
    }
    let horasTeo = document.getElementById('inputHorasTeoricasComp').value;
    let horasPra = document.getElementById('inputHorasPracticasComp').value;
    let carrera = "";
    let carreraSigla = "";
    if (document.getElementById('gridRadios1').checked){
        carrera = "INGENIERIA EN SISTEMAS DE INFORMACION";
        carreraSigla = "IS";
    }else if (document.getElementById('gridRadios2').checked){
        carrera = "INGENIERIA EN TELEMATICA";
        carreraSigla = "IT";
    }else if (document.getElementById('gridRadios3').checked){
        carrera = "TECNICO SUPERIOR EN DESARROLLO WEB";
        carreraSigla = "UA";
    }else if (document.getElementById('gridRadios4').checked){
        carrera = "OFERTA OPTATIVAS DE LA FFCCTT";
        carreraSigla = "SE";
    }else if (document.getElementById('gridRadios5').checked){
        carrera = "TS ESTADISTICA DE SALUD";
        carreraSigla = "SEI";
    }else if (document.getElementById('gridRadios6').checked){
        carrera = "ENFERMERIA";
        carreraSigla = "SEI";
    }else if (document.getElementById('gridRadios7').checked){
        carrera = "BIOANALISIS CLINICO";
        carreraSigla = "SEI";
    }else if (document.getElementById('gridRadios8').checked){
        carrera = "BIOLOGIA";
        carreraSigla = "SEI";
    }
    let ciclo = document.getElementById('inputCicloComp').value;
    let anyo = document.getElementById('inputYearComp').value;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id,
        "nombreC" : nombreComp,
        "modalidad" : modalidadText,
        "horasT" : horasTeo,
        "horasP" : horasPra,
        "carrera" : carrera,
        "siglas" : carreraSigla,
        "ciclo" : ciclo,
        "anyo" : anyo
    };
    $.ajax({
        data: parametros,
        url: route('componentes.update').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha actualizado con exito!', "Componente: " + response.name);
            onRefreshCompList(response.elements);
            document.getElementById('containerViewCompInicio').hidden = false;
            document.getElementById('containerViewCompCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
}

//Eliminar Componente
function btnActionCompDelete(){
    let id = document.getElementById('recipient-comp-delete').value;
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        "id" : id
    };
    $.ajax({
        data: parametros,
        url: route('componentes.delete').template,
        type: 'POST',
        success: function (response) {
            toastr.success('Se ha eliminado con exito!', "Docente: " + response.name);
            onRefreshCompList(response.elements);
            document.getElementById('containerViewCompInicio').hidden = false;
            document.getElementById('containerViewCompCreate').hidden = true;
        },
        error: function (object,type,message){
            if (type == "error"){
                toastr.error(message,capitalizeEachWord(type));
            }else if (type == "timeout"){
                toastr.info(message,capitalizeEachWord(type));
            }else{
                toastr.warning(message,capitalizeEachWord(type));
            }
        }
    });
    $('#exampleModalEliminar').modal('hide');
}

//Actualizar lista de componentes
function onRefreshCompList(list) {
    let containerDelC = document.getElementById('recipient-comp-delete');
    let containerEditC = document.getElementById('recipient-CompEdit');
    containerEditC.innerHTML = "";
    containerDelC.innerHTML = "";
    let part1 = "<option selected>Selecione ...</option>";
    let part2 = "";
    for (let index = 0; index < list.length; index++) {
        part2 += "<option value='" + list[index].id + "'>" + list[index].componente + "</option>";
    }
    containerEditC.innerHTML = part1 + part2;
    containerDelC.innerHTML = part1 + part2;
}