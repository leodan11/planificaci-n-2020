<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComponentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('componentes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('carrera');
            $table->string('nombrecarrera');
            $table->bigInteger('anyo');
            $table->bigInteger('ciclo');
            $table->string('componente');
            $table->string('modalidad');
            $table->bigInteger('horasteoricas');
            $table->bigInteger('horaspracticas');
            $table->bigInteger('asignar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('componentes');
    }
}
