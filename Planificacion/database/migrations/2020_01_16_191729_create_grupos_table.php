<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idcomponente');
            $table->foreign('idcomponente')->references('id')->on('componentes');
            $table->string('tipo');
            $table->bigInteger('numero');
            $table->bigInteger('horas');
            $table->unsignedBigInteger('iddocente');
            $table->foreign('iddocente')->references('id')->on('docentes');
            $table->bigInteger('escargahoraria');
            $table->unsignedBigInteger('idanyos');
            $table->foreign('idanyos')->references('id')->on('anyos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupos');
    }
}
