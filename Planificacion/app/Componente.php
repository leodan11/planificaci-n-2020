<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Componente extends Model
{
    //Actualizar las horas de un componente
    public static function updateHours($id,$ht,$hp){
        DB::update('update grupos set horas = ? where idcomponente = ? and tipo = ?',[$ht,$id,'T']);
        DB::update('update grupos set horas = ? where idcomponente = ? and tipo = ?',[$hp,$id,'P']);
    }

    //Actualizar el componente si es o no asignado
    public static function updateAsignarComponente($id,$value){
        DB::update('update componentes set asignar = ? where id = ?', [$value,$id]);
    }

    //Listar docentes con sus componentes para reporte de docencia directa
    public static function listAllComponentesConDocentesReporte($id){
        $list = DB::table('docentes')->select('idcomponente', 'componente', 'carrera', 'nombrecarrera', 'tipo', 'numero', 'nombre', 
        'grupos.horas as horasgrupo', 'anyo', 'modalidad')
        ->leftJoin('grupos','docentes.id','=','grupos.iddocente')
        ->leftJoin('componentes','grupos.idcomponente', '=', 'componentes.id')
        ->where([
            ['iddocente','!=',32],
            ['grupos.idanyos','=',$id]
        ])
        ->orderBy('carrera')
        ->orderBy('anyo')
        ->orderBy('componente')
        ->orderBy('idcomponente')
        ->orderByDesc('tipo')
        ->orderBy('numero')
        ->get();
        return $list;
    }
}
