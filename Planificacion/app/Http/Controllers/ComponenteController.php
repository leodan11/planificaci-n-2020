<?php

namespace App\Http\Controllers;

use App\Componente;
use App\Docente;
use App\Grupo;
use Illuminate\Http\Request;

class ComponenteController extends Controller
{
    //index
    public function index(){
        $collection = Componente::all();
        return view('componentes.index',compact('collection'));
    }

    //Componetes de IS
    public function IS(){
        $collection = Componente::where('carrera','=','IS')->paginate(6);
        return view('componentes.template',compact('collection'));
    }

    //Componetes de IT
    public function IT(){
        $collection = Componente::where('carrera','=','IT')->paginate(6);
        return view('componentes.template',compact('collection'));
    }

    //Componetes de SE
    public function SE(){
        $collection = Componente::where('carrera','=','SE')->paginate(6);
        return view('componentes.template',compact('collection'));
    }

    //Componetes de UALN
    public function UALN(){
        $collection = Componente::where('carrera','=','UA')->paginate(6);
        return view('componentes.template',compact('collection'));
    }

    //Componente Carga Academica
    public function cargaAcademica(){
        $id = session()->get('IdAnyos');
        $elements = Componente::listAllComponentesConDocentesReporte($id);
        $collection = collect($elements)->toArray();
        return view('componentes.carga_academica',compact('collection'));
    }

    //Buscar un componente
    public function onSearchComp(Request $request){
        if ($request->ajax()){
            $componente = Componente::find($request->id);
            $grupos = Grupo::findByComponent($request->id);
            $docentes = Docente::all();
            return response()->json([
                'componente' => $componente,
                'docentes' => $docentes,
                'grupos' => $grupos
            ]);
        }
    }

    //Actualizar las horas de un componete
    public function updateHoursComp(Request $request){
        if ($request->ajax()){
            $componente = Componente::find($request->id);
            $componente->horasteoricas = $request->T;
            $componente->horaspracticas = $request->P;
            $componente->save();
            Componente::updateHours($request->id,$request->T,$request->P);
            return response()->json([
                'nombre' => $componente->componente
            ]);
        }
    }

    //Obtener Lista de Docentes
    public function ongetDocentes(Request $request){
        if ($request->ajax()){
            $docentes = Docente::all()->whereNotIn('id',32);
            return response()->json([
                'docentes' => $docentes
            ]);
        }
    }

    //Crear un componente
    public function onCrearComponente(Request $request){
        if ($request->ajax()){

            $componente = new Componente();
            $componente->componente = strtoupper($request->nombreC);
            $componente->modalidad = $request->modalidad;
            $componente->horasteoricas = $request->horasT;
            $componente->horaspracticas = $request->horasP;
            $componente->nombrecarrera = $request->carrera;
            $componente->carrera = $request->siglas;
            $componente->ciclo = $request->ciclo;
            $componente->anyo = $request->anyo;
            $componente->asignar = 0;
            $componente->save();

            $list = Componente::all();

            return response()->json([
                'name' => ucwords(strtolower($componente->componente)),
                'elements' => $list
            ]);
        }
    }

    //Buscar solo componente
    public function onSearchComponente(Request $request){
        if ($request->ajax()){
            $componente = Componente::find($request->id);
            return response()->json([
                'componente' => $componente
            ]);
        }
    }

    //Actualizar un componente
    public function onUpdateComponente(Request $request){
        if ($request->ajax()){

            $componente = Componente::find($request->id);
            $componente->componente = strtoupper($request->nombreC);
            $componente->modalidad = $request->modalidad;
            $componente->horasteoricas = $request->horasT;
            $componente->horaspracticas = $request->horasP;
            $componente->nombrecarrera = $request->carrera;
            $componente->carrera = $request->siglas;
            $componente->ciclo = $request->ciclo;
            $componente->anyo = $request->anyo;
            $componente->asignar = 0;
            $componente->save();
            
            $list = Componente::all();

            return response()->json([
                'name' => ucwords(strtolower($componente->componente)),
                'elements' => $list
            ]);
        }
    }


    //Eliminar un componente
    public function onDeleteComponente(Request $request){
        if ($request->ajax()){

            $componente = Componente::find($request->id);
            $name = $componente->componente;
            $componente->delete();

            $list = Componente::all();

            return response()->json([
                'name' => ucwords(strtolower($name)),
                'elements' => $list
            ]);
        }
    }
}
