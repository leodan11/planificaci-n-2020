<?php

namespace App\Http\Controllers;

use App\Anyo;
use App\Aula;
use App\Docente;
use App\Grupo;
use App\Horario;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /* DOCENTES */

    //Obtener todos los docentes
    public function onGetTeachersAll(){
        return response()->json(Docente::all());
    }

    //Obtener horario de un docente por su id
    public function onGetTeacherSchedule($cycle, $id){
        //Verficamos que los campos recibidos no sean negativos
        if($cycle > 0 && $id > 0){
            $schedule = null;
            //Obtenemos todos los docentes registrados
            $teachers = Docente::all();
            //Si solicitan todos
            $count = count($teachers);
            if($id == 999){
                for ($i = 0; $i < $count; $i++){
                    $schedule[$i]["id"] = $teachers[$i]->id;
                    $schedule[$i]["nombre"] = $teachers[$i]->nombre;
                    $schedule[$i]["descripcion"] = $teachers[$i]->contratacion;
                    $schedule[$i]["horario"] = Horario::listHorariosDocentes($teachers[$i]->id)->whereIn('cycle',$cycle);
                }
            }else{
                //Un dato en especifico
                foreach ($teachers as $key ) {
                    if ($key->id == $id) {
                        $schedule[0]["id"] = $key->id;
                        $schedule[0]["nombre"] = $key->nombre;
                        $schedule[0]["descripcion"] = $key->contratacion;
                        $schedule[0]["horario"] = Horario::listHorariosDocentes($key->id)->whereIn('cycle',$cycle);
                    }
                }
            }
            return response()->json($schedule);
        }else{
            return response()->json(null);
        }
    }

    /* AULAS */
    
    //Obtener todos las aulas
    public function onGetClassroomsAll(){
        $collection = Aula::all();
        return response()->json($collection);
    }

    //Obtener horario de la aula por su id
    public function onGetClassroomSchedule($cycle, $id){
        //Verficamos que los campos recibidos no sean negativos
        if($cycle > 0 && $id > 0){
            $schedule = null;
            //Obtenemos todas las aulas registrados
            $classrooms = Aula::all();
            //Si solicitan todos
            $count = count($classrooms);
            if($id == 999){
                for ($i = 0; $i < $count; $i++){
                    $schedule[$i]["id"] = $classrooms[$i]->id;
                    $schedule[$i]["nombre"] = $classrooms[$i]->nombre;
                    $schedule[$i]["descripcion"] = $classrooms[$i]->tipo;
                    $schedule[$i]["horario"] = Horario::listHorarioAula($teachers[$i]->id)->whereIn('cycle',$cycle);
                }
            }else{
                //Un dato en especifico
                foreach ($classrooms as $key ) {
                    if ($key->id == $id) {
                        $schedule[0]["id"] = $key->id;
                        $schedule[0]["nombre"] = $key->nombre;
                        $schedule[0]["descripcion"] = $key->tipo;
                        $schedule[0]["horario"] = Horario::listHorarioAula($key->id)->whereIn('cycle',$cycle);
                    }
                }
            }
            return response()->json($schedule);
        }else{
            return response()->json(null);
        }
    }

    /* AÑOS Y CARRERA */

    //Obtener horario por carrera y año lectivo
    public function onGetProfessionAnyosSchedule($cycle, $profession, $id){
        //Verficamos que los campos recibidos no sean negativos
        if($cycle > 0 && $id > 0){
            //Generamos variables
            $schedule = null;
            //Si se solicitan todos los datos
            if($id == 999){
                $anyo = 1;
                for ($i = 0; $i < 5; $i++){
                    $schedule[$i]["id"] = $anyo;
                    $schedule[$i]["horario"] = Horario::listHorariosAnyo($profession,$anyo)->whereIn('cycle',$cycle);
                    $anyo += 1;
                }
            }else{
                $schedule[0]["id"] = $id;
                $schedule[0]["horario"] = Horario::listHorariosAnyo($profession,$id)->whereIn('cycle',$cycle);
            }
            return response()->json($schedule);
        }else{
            return response()->json(null);
        }
    }

    /* CICLO ACADÉMICO */
    
    //Obtener todos los ciclos academicos
    public function onGetCyclesAll(){
        return response()->json(Anyo::all());
    }

    /* Ciclo Academico */
    
    //Obtener todos los horarios
    public function onGetSchedulesAll(){
        return response()->json(Horario::all());
    }

}
