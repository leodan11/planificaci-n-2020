<?php

namespace App\Http\Controllers;

use App\Aula;
use App\Docente;
use App\Grupo;
use App\Horario;
use Illuminate\Http\Request;

class HorarioController extends Controller
{
    //Index Horario
    public function index(){
        return view('horarios.index');
    }

    //Manejar los horarios
    public function onGetlist(){
        $aulas = Aula::all();
        return view('horarios.crear_horarios',compact('aulas'));
    }

    //Obtener horario de aula especifica
    public function onSetlist(Request $request){
        if ($request->ajax()) {
            $aula = Aula::find($request->id);
            $grupos = Grupo::listAllGruposParaHorario();
            $horario = Horario::listHorarioAula($request->id);
            $horariosAulas = null;
            $horariosAulas['id'] = $aula->id;
            $horariosAulas['name'] = $aula->nombre;
            $horariosAulas['schedule'] = $horario;
            return response()->json([
                'horarioAula' => $horariosAulas,
                'grups' => $grupos
            ]);
        }
    }

    //Agregar, Eliminar o actualizar un componente en un aula
    public function onSetScheduleClassRoom(Request $request){
        if ($request->ajax()) {
            $title = "";
            $message = "";
            if ($request->evento == "add") {
                $horas = ($request->idGrupo > 0)? 2 : 0;
                $horario = new Horario();
                $horario->idaula = $request->idAula;
                $horario->dia = $request->dia;
                $horario->periodo = $request->periodo;
                $horario->idgrupo = $request->idGrupo;
                $horario->horas = $horas;
                $horario->index = $request->index;
                $horario->idanyos = session()->get('IdAnyos');
                $horario->save();
                $title = "Agregado!";
                $message = "Se a agregado con exito!";
            } else if($request->evento == "delete"){
                $horario = Horario::find($request->idHorario);
                $horario->delete();
                $title = "Eliminado!";
                $message = "Se a eliminado con exito!";
            }
            
            return response()->json([
                "title" => $title,
                "message" => $message,
                "aula" => $request->idAula,
                "grupo" => $request->idGrupo
            ]);
        }
    }


    //
    public function onListHorario(Request $request){
        if ($request->ajax()) {
            $aulas = Aula::all();
            $grupos = Grupo::listAllGruposParaHorario();
            $horariosAulas = null;
            $temp = collect($aulas)->toArray();
            for($i = 0; $i < count($temp); $i++)
            {
                $horariosAulas[$temp[$i]['id']]['id'] = $temp[$i]['id'];
                $horariosAulas[$temp[$i]['id']]['nombre'] = $temp[$i]['nombre'];
                $horariosAulas[$temp[$i]['id']]['datos'] = Horario::listHorarioAula($temp[$i]['id']);
            }
            return response()->json([
                'horarioAula' => $horariosAulas
            ]);
        }
    }


    //Horario Docente
    public function onDocenteHr(){
        $collection = Docente::all();
        return view('horarios.docente_horario',compact('collection'));
    }

    public function onDocenteHrView(Request $request){
        if($request->ajax()){
            $idSelect = $request->iddocente;
            //Si se solicitan todos
            if($idSelect == 100){
                $number = Docente::all()->whereNotIn('id',32);
                $filas= collect($number)->toArray();
                for ($i=0; $i < count($filas); $i++) { 
                    $collection[$i]['id'] = $number[$i]->id;
                    $collection[$i]['nombre'] = $number[$i]->nombre;
                    $collection[$i]['data'] = Horario::listHorariosDocentes($number[$i]->id);
                }
            }else{
                $collection[0]['id'] = $idSelect;
                $collection[0]['nombre'] = "";
                $collection[0]['data'] = Horario::listHorariosDocentes($idSelect);
            }
            return response()->json([
                "collection" => $collection
            ]);
        }
    }

    //Horario Año
    public function onAnyoHr(){
        return view('horarios.anyo_horario');
    }

    public function onAnyoHrView(Request $request){
        if($request->ajax()){
            $carrera = $request->carrera;
            $anyo = $request->anyo;
            //Si se solicitan todos
            if($anyo == 100){
                for ($i=0; $i < 5; $i++) {
                    $id = $i + 1;
                    $collection[$i]['id'] = $id;
                    $collection[$i]['data'] = Horario::listHorariosAnyo($carrera,$id);
                }
            }else if($anyo > 0 && $anyo < 100){
                $collection[0]['id'] = $anyo;
                $collection[0]['data'] = Horario::listHorariosAnyo($carrera,$anyo);
            }
            return response()->json([
                "collection" => $collection
            ]);
        }
    }

    //Horario Aula
    public function onAulahr(){
        $collection = Aula::all();
        return view('horarios.aula_horario',compact('collection'));
    }

    public function onAulaHrView(Request $request){
        if($request->ajax()){
            $idSelect = $request->idaula;
            //Si se solicitan todos
            if($idSelect == 100){
                $number = Aula::all();
                $filas= collect($number)->toArray();
                for ($i=0; $i < count($filas); $i++) { 
                    $collection[$i]['id'] = $number[$i]->id;
                    $collection[$i]['nombre'] = $number[$i]->nombre;
                    $collection[$i]['data'] = Horario::listHorarioAula($number[$i]->id);
                }
            }else{
                $collection[0]['id'] = $idSelect;
                $collection[0]['nombre'] = "";
                $collection[0]['data'] = Horario::listHorarioAula($idSelect);
            }
            return response()->json([
                "collection" => $collection
            ]);
        }
    }   
}