<?php

namespace App\Http\Controllers;

use App\Aula;
use Illuminate\Http\Request;

class AulaController extends Controller
{
    //Index
    public function index(){
        $collection = Aula::all();
        return view('aulas.index',compact('collection'));
    }

    //Crear una Aula
    public function onCrearAula(Request $request){
        if ($request->ajax()){
            $aula = new Aula();
            $aula->nombre = strtoupper($request->name);
            $aula->tipo = $request->tipo;
            $aula->capacidad = $request->capacidad;
            $aula->save();

            $list = Aula::all();

            return response()->json([
                'name' => ucwords(strtolower($aula->nombre)),
                'elements' => $list
            ]);
        }
    }

    //Buscar una aula
    public function onSearchAula(Request $request){
        if ($request->ajax()) {
            $aula = Aula::find($request->id);

            return response()->json([
                'aula' => $aula
            ]);
        }
    }

    //Actualizar una aula
    public function onUpdateAula (Request $request){
        if ($request->ajax()) {
            $aulaUp = Aula::find($request->id);
            $aulaUp->nombre = strtoupper($request->name);
            $aulaUp->tipo = $request->tipo;
            $aulaUp->capacidad = $request->capacidad;
            $aulaUp->save();

            return response()->json([
                'name' => ucwords(strtolower($aulaUp->nombre))
            ]);
        }
    }

    //Eliminar una aula
    public function onDeleteAula (Request $request){
        if ($request->ajax()) {
            $aula = Aula::find($request->id);
            $nombre = $aula->nombre;
            $aula->delete();

            $list = Aula::all();

            return response()->json([
                'name' => ucwords(strtolower($nombre)),
                'elements' => $list
            ]);
        }
    }
}
